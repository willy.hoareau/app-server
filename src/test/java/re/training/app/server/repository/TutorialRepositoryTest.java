package re.training.app.server.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import re.training.app.server.model.Tutorial;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@DataJpaTest
class TutorialRepositoryTest {

    @Autowired
    TutorialRepository tutorialRepository;

    @Test
    void findByPublished() {
        Tutorial tutorial = new Tutorial();
        tutorial.setDescription("This is a test");
        tutorial.setPublished(true);
        tutorial.setTitle("TestTPublished");
        tutorialRepository.save(tutorial);

        Tutorial tutorialNotPublished = new Tutorial();
        tutorialNotPublished.setDescription("This is another test");
        tutorialNotPublished.setPublished(false);
        tutorialNotPublished.setTitle("TestUNotPublished");
        tutorialRepository.save(tutorialNotPublished);

        List<Tutorial> list = tutorialRepository.findByPublished(true);
        assertThat(list, hasSize(1));
        assertThat(list.get(0).getDescription(), is(equalTo("This is a test")));
        assertThat(list.get(0).getTitle(), is(equalTo("TestTPublished")));
        assertThat(list.get(0).isPublished(), is(true));

        List<Tutorial> listNotPublished = tutorialRepository.findByPublished(false);
        assertThat(listNotPublished, hasSize(1));
        assertThat(listNotPublished.get(0).getDescription(), is(equalTo("This is another test")));
        assertThat(listNotPublished.get(0).getTitle(), is(equalTo("TestUNotPublished")));
        assertThat(listNotPublished.get(0).isPublished(), is(false));

    }

    @Test
    void findByTitleContaining() {
    }
}